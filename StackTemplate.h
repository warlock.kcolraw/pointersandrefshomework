#pragma once
#include <iostream>
#include <iomanip>
#include <type_traits>

template <class ElementType>
class StackTemplate
{
private:
	ElementType* Array;
	int Size;
public:
	StackTemplate() : Array(nullptr), Size(0) {}
	~StackTemplate() { delete[] Array; }

	// Shows a size of a stack
	int GetSize() { return Size; }

	// Pops the last element from a stack
	ElementType Pop()
	{
		ElementType Value;

		switch (Size)
		{
		case 0:
			if (std::is_same<ElementType, std::string>::value)
			{
				return {};
			}
			else
			{
				return NULL;
			}
		case 1:
			Value = Array[0];
			delete[] Array;
			Array = nullptr;
			break;
		default:
			Value = Array[Size - 1];
			ElementType* NewArray = new ElementType[Size - 1];
			std::copy(Array, Array + Size - 1, NewArray);
			delete[] Array;
			Array = NewArray;
		}
		Size -= 1;
		return Value;
	}

	// Pushes a value to a stack
	void Push(ElementType Value)
	{
		if (Array)
		{
			ElementType* NewArray = new ElementType[Size + 1];
			std::copy(Array, Array + Size, NewArray);
			delete[] Array;
			Array = NewArray;
			Array[Size] = Value;
		}
		else
		{
			Array = new ElementType[1];
			Array[0] = Value;
		}
		Size += 1;
	}

	// Prints all elements of a stack into console
	void ShowElements()
	{
		if (Array)
		{
			std::cout << "Current stack: ";
			for (int i = 0; i < Size; i++)
			{
				std::cout << (i > 0 ? ", " : "") << std::fixed << Array[i];
			}
			std::cout << std::endl;
		}
		else
		{
			std::cout << "Stack is empty!\n";
		}
	}
};