#include "Stack.h"
#include "StackTemplate.h"
#include <iostream>
#include <string>

int main()
{
	Stack NewStack;
	NewStack.ShowElements();
	NewStack.Push(1.0f);
	NewStack.ShowElements();
	NewStack.Push(2.0f);
	NewStack.ShowElements();
	std::cout << NewStack.Pop() << std::endl;
	NewStack.ShowElements();
	std::cout << NewStack.Pop() << std::endl;
	NewStack.ShowElements();
	std::cout << NewStack.Pop() << std::endl;

	std::cout << "--------------------" << std::endl;

	StackTemplate <int> StackInt;
	StackInt.ShowElements();
	StackInt.Push(1);
	StackInt.ShowElements();
	StackInt.Push(2);
	StackInt.ShowElements();
	std::cout << StackInt.Pop() << std::endl;
	StackInt.ShowElements();
	std::cout << StackInt.Pop() << std::endl;
	StackInt.ShowElements();
	std::cout << StackInt.Pop() << std::endl;

	std::cout << "--------------------" << std::endl;

	StackTemplate <std::string> StackString;
	StackString.ShowElements();
	StackString.Push("ab");
	StackString.ShowElements();
	StackString.Push("cd");
	StackString.ShowElements();
	std::cout << StackString.Pop() << std::endl;
	StackString.ShowElements();
	std::cout << StackString.Pop() << std::endl;
	StackString.ShowElements();
	std::cout << StackString.Pop() << std::endl;

	return 0;
}