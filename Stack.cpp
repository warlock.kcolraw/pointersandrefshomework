#include "Stack.h"
#include <iostream>
#include <iomanip>

float Stack::Pop()
{
	float Value;

	switch (Size)
	{
	case 0:
		return NULL;
	case 1:
		Value = Array[0];
		delete[] Array;
		Array = nullptr;
		break;
	default:
		Value = Array[Size - 1];
		float* NewArray = new float[Size - 1];
		std::copy(Array, Array + Size - 1, NewArray);
		delete[] Array;
		Array = NewArray;
	}
	Size -= 1;
	return Value;
}

void Stack::Push(float Value) 
{
	if (Array)
	{
		float* NewArray = new float[Size + 1];
		std::copy(Array, Array + Size, NewArray);
		delete[] Array;
		Array = NewArray;
		Array[Size] = Value;
	}
	else
	{
		Array = new float[1];
		Array[0] = Value;
	}
	Size += 1;
}

void Stack::ShowElements()
{
	if (Array)
	{
		std::cout << "Current stack: ";
		for (int i = 0; i < Size; i++)
		{
			std::cout << (i > 0 ? ", " : "") << std::fixed << std::setprecision(2) << Array[i];
		}
		std::cout << std::endl;
	}
	else
	{
		std::cout << "Stack is empty!\n";
	}
}