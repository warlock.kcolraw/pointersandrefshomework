#pragma once

class Stack
{
private:
	float* Array;
	int Size;
public:
	Stack() : Array(nullptr), Size(0) {}
	~Stack() { delete[] Array; }

	// Shows a size of a stack
	int GetSize() { return Size; }

	// Pops the last element from a stack
	float Pop();

	// Pushes a value to a stack
	void Push(float Value);

	// Prints all elements of a stack into console
	void ShowElements();
};

